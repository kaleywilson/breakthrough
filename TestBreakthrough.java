import org.junit.*;

import static org.junit.Assert.*;

/** Initial test case class for Breakthrough
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/
public class TestBreakthrough {
  Breakthrough game;
  /** Fixture */
  @Before
  public void setUp() {
    game = new BreakthroughImpl();
  }

  @Test
  public void shouldHaveBlackPawnOn00(){
    assertEquals( "Black has pawn on (0,0)",
                  BreakthroughImpl.PieceType.BLACK, game.getPieceAt(0,0) );
  }
  
  @Test
  public void shouldHaveWhitePawn70() {
	  assertEquals("White has pawn on (7,0)", 
			  BreakthroughImpl.PieceType.WHITE, game.getPieceAt(7, 0));
  }
  
  /**
   * Tested various moves that are not allowed by the rules of the game.
   * Also tested with both the white and black player
   * created some more pieces to test moving into a the same color or a
   * different color in different situations
   */
  @Test
  public void invalidMoves() {
	  game.placePiece(3,3, BreakthroughImpl.PieceType.BLACK);
	  game.placePiece(5, 2, BreakthroughImpl.PieceType.BLACK);
	  
	  //conduct tests with black player
	  assertFalse("Pawn cannot move forward into a taken space by same color",
			  game.isMoveValid(0, 1, 1, 1));
	  assertFalse("Pawn cannot move forward into a taken space by opposite color",
			  game.isMoveValid(2, 2, 3, 2));
	  assertFalse("Pawn cannot move backward",
			  game.isMoveValid(5, 2, 4, 2));
	  assertFalse("Pawn cannot move backward diagnoal",
			  game.isMoveValid(5, 2, 4, 1));
	  assertFalse("Cannot move from outside board",
			  game.isMoveValid(9, 1, 8, 0));
	  
	  game.changePlayer();	//conduct tests with white player
	  assertFalse("Pawn cannot move more than one space forward",
			  game.isMoveValid(6, 0, 3, 0));
	  assertFalse("Pawn cannot move diagnol into a taken space by same color",
			  game.isMoveValid(7, 3, 6, 2));
	  assertFalse("Pawn cannot move more than one space diagnol",
			  game.isMoveValid(6, 4, 4, 2));
	  assertFalse("Pawn cannot move off board",
			  game.isMoveValid(7, 0, 6, -1));
	  assertFalse("Pawn cannot move no spaces", 
			  game.isMoveValid(7,  0, 7, 0));

	  }
  /**
   * testing various valid moves with both the black
   * and white players. Placed some more pieces across the board
   * to do more accurate testing
   */
  @Test
  public void validMoves() {
	  game.placePiece(2,3, BreakthroughImpl.PieceType.WHITE);
	  game.placePiece(5, 2, BreakthroughImpl.PieceType.BLACK);
	  
	  //conducts testing with black player
	  assertTrue("Pawn can move forward into empty space",
			  game.isMoveValid(1, 6, 2, 6));
	  assertTrue("Pawn can move diagnol into a taken space by opposite color",
				 game.isMoveValid(1, 2, 2, 3));
	  assertTrue("Pawn can move diagnol into an empty space",
			  game.isMoveValid(1, 6, 2, 5));
	  
	  //conducts testing with white player
	  game.changePlayer();
	  assertTrue("Pawn can move forward into empty space",
			  game.isMoveValid(6, 4, 5, 4));
	  assertTrue("Pawn can move diagnol into a taken space by opposite color",
				 game.isMoveValid(6, 3, 5, 2));
	  assertTrue("Pawn can move diagnol into an empty space",
			  game.isMoveValid(6, 6, 5, 7));
  }

  /**
   * These tests expect a certain piece at a position after a move depending
   * on whether the move is valid or not. The test goes back between white 
   * and black players like a real game but I placed a few more pieces to
   * speed up the tests and so they don't depend on eachother. Also it 
   * tests if it changes players correctly when the player does an invalid 
   * or valid move which is the only dependency in the tests. 
   */
  @Test
  public void testingMoves() {
	  game.placePiece(2, 4, BreakthroughImpl.PieceType.WHITE);
	  game.placePiece(5, 2, BreakthroughImpl.PieceType.BLACK);
	  
	  
	  game.move(0, 1, 1, 1);
	  assertEquals("Will not move the piece, still Black Player's turn",
			  BreakthroughImpl.PlayerType.BLACK, game.getPlayerInTurn());
	  game.move(1,7,2,7);
	  assertEquals("Black Piece moves to forward 2, 7",
			  BreakthroughImpl.PieceType.BLACK, game.getPieceAt(2, 7));
	  assertEquals("White Player's turn",
			  BreakthroughImpl.PlayerType.WHITE, game.getPlayerInTurn());
	  assertEquals("No piece is at 1, 7",
			  BreakthroughImpl.PieceType.NONE, game.getPieceAt(1, 7));
	  game.move(6, 5, 5, 4);
	  assertEquals("White Piece moves to diagnol to 5, 4",
			  BreakthroughImpl.PieceType.WHITE, game.getPieceAt(5, 4));
	  assertEquals("No piece is at 6, 5",
			  BreakthroughImpl.PieceType.NONE, game.getPieceAt(6, 5));
	  assertEquals("Black Player's turn",
			  BreakthroughImpl.PlayerType.BLACK, game.getPlayerInTurn());
	  game.move(1, 5, 2, 4);
	  assertEquals("Black piece replaces the white place when moved diagnol",
			  BreakthroughImpl.PieceType.BLACK, game.getPieceAt(2, 4));
	  game.move(6,2, 5, 2);
	  assertEquals("Black piece is still in place if white piece tries to go forward",
			  BreakthroughImpl.PieceType.BLACK, game.getPieceAt(5, 2));
	  assertEquals("White piece is still in its original place",
			  BreakthroughImpl.PieceType.WHITE, game.getPieceAt(6, 2));
  }
  
  /**
   * This final test checks if it recognizes a winner. I replaced pieces manually to get
   * a result for both white and black players
   */
  @Test
  public void testWinning() {
	  game.placePiece(1, 2, Breakthrough.PieceType.WHITE);
	  game.placePiece(0, 2, Breakthrough.PieceType.NONE);
	  game.placePiece(6, 4, Breakthrough.PieceType.BLACK);
	  game.placePiece(7, 4, Breakthrough.PieceType.NONE);
	  
	  game.move(1, 6, 2, 6);
	  assertEquals("No one is the winner yet", 
			  null, game.getWinner());
	  game.move(1, 2, 0, 2);
	  assertEquals("Winner is White Player", 
			  BreakthroughImpl.PlayerType.WHITE, game.getWinner());
	  game.placePiece(0,2, Breakthrough.PieceType.NONE);	//erase white's piece to check black player
	  game.move(6, 4, 7, 4);
	  assertEquals("Winner is Black Player", 
			  BreakthroughImpl.PlayerType.BLACK, game.getWinner());
	  
	  
  }

}
