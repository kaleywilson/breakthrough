/**
 * Implementation stub.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */
public class BreakthroughImpl implements Breakthrough {
	PieceType[][] board;
	PlayerType currentPlayer;

	public BreakthroughImpl() {
		board = new PieceType[8][8];
		currentPlayer = PlayerType.BLACK;
		for (int i = 0; i <= 1; i++) {
			for (int j = 0; j <= 7; j++) {
				board[i][j] = PieceType.BLACK;
			}
		}
		for (int i = 6; i <= 7; i++) {
			for (int j = 0; j <= 7; j++) {
				board[i][j] = PieceType.WHITE;
			}
		}
		for (int i = 2; i <= 5; i++) {
			for (int j = 0; j <= 7; j++) {
				board[i][j] = PieceType.NONE;
			}
		}
	}
	public PieceType getPieceAt(int row, int column) {
		return board[row][column];
	}

	public PlayerType getPlayerInTurn() {
		return currentPlayer;
	}

	public PlayerType getWinner() {
		for(int i = 0; i <= 7; i++) {
			if(getPieceAt(0, i).name() == PlayerType.WHITE.name()) {
				return PlayerType.WHITE;
			}else if(getPieceAt(7, i).name() == PlayerType.BLACK.name()) {
				return PlayerType.BLACK;
			}
		}
		return null;
	}

	public boolean isMoveValid(int fromRow, int fromColumn, int toRow, int toColumn) {
		try {
			String from = getPieceAt(fromRow, fromColumn).name();
			String to = getPieceAt(toRow, toColumn).name();
			String player = currentPlayer.name();

			if (from.equals(player)) {
				if (currentPlayer == PlayerType.BLACK) {
					if (toRow - fromRow == 1 && toColumn - fromColumn == 0) {
						if (to.equals(PieceType.NONE.name())) {
							return true;
						} else {
							return false;
						}
					} else if ((toRow - fromRow == 1 && (toColumn-fromColumn==1||toColumn-fromColumn==-1))) {
						if (to.equals(PieceType.NONE.name())) {
							return true;
						} else if (to.equals(player)) {
							return false;
						} else {
							return true;
						}
					} else {
						return false;
					}
				} else if (currentPlayer == PlayerType.WHITE) {
					if (toRow - fromRow == -1 && toColumn - fromColumn == 0) {
						if (to.equals(PieceType.NONE.name())) {
							return true;
						} else {
							return false;
						}
					} else if ((toRow - fromRow == -1 && (toColumn-fromColumn==1||toColumn-fromColumn==-1))) {
						if (to.equals(PieceType.NONE.name())) {
							return true;
						} else if (to.equals(player)) {
							return false;
						} else {
							return true;
						}
					} else {
						return false;
					}
				}
			}
			return false;
		} catch (ArrayIndexOutOfBoundsException e) {
			return false;
		}
	}

	public void move(int fromRow, int fromColumn, int toRow, int toColumn) {
		if(isMoveValid(fromRow, fromColumn, toRow, toColumn)) {
			if(currentPlayer == PlayerType.BLACK) {
				board[toRow][toColumn] = PieceType.BLACK;
			} else {
				board[toRow][toColumn] = PieceType.WHITE;
			}
			board[fromRow][fromColumn] = PieceType.NONE;
			System.out.println("Player " + currentPlayer.name() + " moved from (" +
					fromRow + ", " + fromColumn + ") to (" + toRow + ", " + toColumn + ").");
			if(getWinner() == PlayerType.BLACK) {
				System.out.println("Black Player Wins!!");
			}else if(getWinner() == PlayerType.WHITE) {
				System.out.println("White Player Win!!");
			}
			changePlayer();
		}else {
			System.out.println("That move is invalid, try again");
		}
	}
	
	/** 
	 * places piece on board with given row and column
	 */
	public void placePiece(int row, int column, PieceType piece) {
		board[row][column] = piece;
	}
	
	public void changePlayer() {
		if(currentPlayer == PlayerType.WHITE) {
			currentPlayer = PlayerType.BLACK;
		}else {
			currentPlayer = PlayerType.WHITE;
		}
	}
}
